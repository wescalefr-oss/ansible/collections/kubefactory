env:
	pip3 install -U --no-cache pip setuptools wheel
	pip3 install -U --no-cache -r requirements.txt 
	ansible-galaxy collection install -fr requirements.yml

core:
	[ -n "${KUBEFACTORY_WORKSPACE}" ] && \
	ansible-playbook playbooks/tf_core.yml && \
	ansible-playbook playbooks/core_init.yml && \
	ansible-playbook playbooks/gandi_delegate_subdomain.yml -e mode=destroy -e force=true && \
	ansible-playbook playbooks/gandi_delegate_subdomain.yml && \
	ansible-playbook playbooks/core_system.yml

rancher:
	[ -n "${KUBEFACTORY_WORKSPACE}" ] && \
	ansible-playbook rtnp.galaxie_clans.install_role -e scope=${KUBEFACTORY_WORKSPACE} -e role_name=k3s

rancher_bootstrap:
	[ -n "${KUBEFACTORY_WORKSPACE}" ] && \
	ansible-playbook playbooks/tf_rancher_bootstrap.yml

core-reconf:
	[ -n "${KUBEFACTORY_WORKSPACE}" ] && \
	ansible-playbook playbooks/gandi_delegate_subdomain.yml -e mode=destroy -e force=true && \
	ansible-playbook playbooks/gandi_delegate_subdomain.yml && \
	ansible-playbook playbooks/core_system.yml

core-destroy:
	[ -n "${KUBEFACTORY_WORKSPACE}" ] && \
	ansible-playbook playbooks/gandi_delegate_subdomain.yml -e mode=destroy -e force=true && \
	ansible-playbook playbooks/tf_core.yml -e tf_action=destroy

kapsule:
	[ -n "${KUBEFACTORY_WORKSPACE}" ] && \
	[ -n "${KUBEFACTORY_KAPSULE_NAME}" ] && \
	ansible-playbook playbooks/tf_kapsule.yml && \
	ansible-playbook playbooks/tf_kapsule_import.yml
#	ansible-playbook playbooks/tf_kapsule_tooling.yml

kapsule-destroy:
	[ -n "${KUBEFACTORY_WORKSPACE}" ] && \
	[ -n "${KUBEFACTORY_KAPSULE_NAME}" ] && \
	ansible-playbook playbooks/tf_kapsule_tooling.yml -e tf_action=destroy && \
	ansible-playbook playbooks/tf_kapsule_import.yml -e tf_action=destroy && \
	ansible-playbook playbooks/tf_kapsule.yml -e tf_action=destroy
