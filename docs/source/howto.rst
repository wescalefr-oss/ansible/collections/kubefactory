##########################################
How-to Guides
##########################################

.. toctree::
   :maxdepth: 1

   howto/get_started
   howto/conf_scw
   howto/conf_gandi
   howto/create_kapsule
