##########################################
Manage a Kapsule cluster
##########################################

Kapsule is a Kubernetes cluster as-a-services managed by Scaleway. We will see here how to 
start one and integrate it into our Rancher instance so it can be managed by it.

Prerequisites
==========================================

* Have a ``KUBEFACTORY_WORKSPACE`` environment variable set. (Let's say it has a value of ``myworkspace``)
* Choose a kapsule name, let's says ``mykapsule``
* Export it in your terminal so playbooks can rely on its name.

.. code:: bash

    export KUBEFACTORY_KAPSULE_NAME=mykapsule

Create & import in Rancher
==========================================

Run:

.. code:: bash

    ansible-playbook playbooks/tf_kapsule.yml
    ansible-playbook playbooks/tf_kapsule_import.yml

.. and wait for the cluster to be in status ``Active`` in Rancher.

Stuff up your cluster
==========================================

This command will install on your cluster:

* `Rancher monitoring <https://rancher.com/docs/rancher/v2.x/en/monitoring-alerting/v2.5/>`_
* `Rancher logging <https://rancher.com/docs/rancher/v2.x/en/logging/v2.5/>`_
* `cert-manager <https://cert-manager.io/docs/>`_
* `nginx-ingress <https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/>`_

.. code:: bash

    ansible-playbook playbooks/tf_kapsule_tooling.yml

After this, you should find a file at ``host_vars/myworkspace/tf_kapsules_tooling.yml`` with a single dict
``kapsules_tooling`` and an entry for your kapsule instance. Like this:

.. code:: yaml

    kapsules_tooling:
    # BEGIN -- myworkspace -- mykapsule
      mykapsule:
        load_balancer_ip: "10.10.10.10"
    # END -- myworkspace -- mykapsule

.. attention::

    Of course, the value of the ip will be different for you.

Point a DNS to your cluster entry point
==========================================

To maximize your chances of successful application deployments and exposition, you must now point a DNS record to
your nginx-ingress load balancer.

Edit ``host_vars/myworkspace/system.yml`` to make it look like:

.. code:: yaml

    # [...]
    bind_zone_domains:
      - name: "{{ system_base_domain }}"
        type: master
    # [...]
        hosts:
    # [...]
          - name: mykapsule
            ip: "{{ kapsules_tooling.mykapsule.load_balancer_ip }}"
    # Every CNAME your like
            aliases:
              - ci
              - monitor
              - app

and *apply* your changes by running:

.. code:: bash

    ansible-playbook playbooks/core_system.yml

.. admonition:: CONGRATULATIONS
    :class: important

    You now have DNS pointing at a well configured Kapsule cluster. You should be ready 
    to handle applications deployments.

----

.. admonition:: SEASONED SCRIPT - CREATE
    :class: danger

    .. code:: bash

        export KUBEFACTORY_KAPSULE_NAME=mykapsule &&
        ansible-playbook playbooks/tf_kapsule.yml && \
        ansible-playbook playbooks/tf_kapsule_import.yml && \
        sleep 600 && \
        ansible-playbook playbooks/tf_kapsule_tooling.yml
        
----

.. admonition:: SEASONED SCRIPT - DESTROY
    :class: danger

    .. code:: bash

        ansible-playbook playbooks/tf_kapsule_tooling.yml -e tf_action=destroy && \
        ansible-playbook playbooks/tf_kapsule_import.yml -e tf_action=destroy && \
        ansible-playbook playbooks/tf_kapsule.yml -e tf_action=destroy
