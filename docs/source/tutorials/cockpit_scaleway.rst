##########################################
Install Rancher@Scaleway
##########################################

This tutorial will guide you through:

* Creating a server on Scaleway Elements
* Delegating a Gandi-managed DNS subdomain authority to it
* Installing K3S and Rancher

******
Ready?
******

For this tutorial to go as expected you **must**:

* Follow the :doc:`/howto/get_started` how-to guide
* Follow the :doc:`/howto/conf_scw` how-to guide
* Follow the :doc:`/howto/conf_gandi` how-to guide
* Choose your Rancher instance name. It must be a **valid dns subdomain**. 

******
Set.
******

For this tutorial, let's assume that:

* your domain is ``yourdomain.fr``
* your Rancher instance is ``yourlab``

******
Go!
******

Setup the Rancher instance name
==========================================

Most of the playbooks hosts scope is based on the ``'workspace'`` ansible variable.
This variable take its default value from environment variable ``KUBEFACTORY_WORKSPACE``.

This value will be used as a DNS subdomain delegated to the Scaleway instance we will create.

Run:

.. code:: bash

    export KUBEFACTORY_WORKSPACE="yourlab"

Create instance
==========================================


Run:

.. code:: bash

    ansible-playbook playbooks/tf_core.yml

After that you will find new file generated in the directory ``host_vars/${KUBEFACTORY_WORKSPACE}``:

* a ``terraform/core`` subdirectory: sources for Terraform provisioning Scaleway instance
* a ``secrets`` subdirectory: contains generated keys for instance SSH access
* ``system.yml``: contains configuration for ``galaxie-clans`` roles
* ``tf_core.yml``: contains output values coming from ``terraform/core`` apply

Init server
==========================================

Run:

.. code:: bash

    direnv reload
    ansible-playbook playbooks/core_init.yml

This step will access to your unprepared server and normalize a technical user for 
ansible to connect with in the following steps.

You can check everything went ok by running:

.. code:: bash

    $ ssh -F ssh.cfg ${KUBEFACTORY_WORKSPACE} 2>/dev/null whoami
    caretaker


Delegate a subdomain
==========================================

Run:

.. code:: bash

    ansible-playbook playbooks/gandi_delegate_subdomain.yml


Provision system
==========================================

Run:

.. code:: bash

    ansible-playbook playbooks/core_system.yml

This will clean configure your server. It also installs a Bind service to server as authority
on the subdomain you delegated the step before. Configuration vars impacting roles behavior are
located in ``host_vars/${KUBEFACTORY_WORKSPACE}/system.yml``.


Provision certificates
==========================================

Run:

.. code:: bash

    ansible-playbook playbooks/acme_rotate_certificates.yml

This step will retrieve valid certificates from LetsEncrypt with a DNS challenge based on our 
Bind service.

Provision Rancher
==========================================

Run:

.. code:: bash

    ansible-playbook playbooks/core_rancher.yml

This step will install Rancher by itself, based on the official Helm chart, and expose it
behind the generated certificates. At this stage it is wide open to any user.

Run this to generate a first admin password:

.. code:: bash

    ansible-playbook playbooks/tf_rancher_bootstrap.yml

The default user is ``admin`` and the password has been generated under the file 
``host_vars/${KUBEFACTORY_WORKSPACE}/secrets/rancher_admin.password``

----

.. admonition:: CONGRATULATIONS
    :class: important

    Your Rancher is setup! You can reach it at `<https://rancher.k3s.yourlab.yourdomain.fr>`_

----

.. admonition:: SEASONED SCRIPT
    :class: danger

    .. code:: bash

        export KUBEFACTORY_WORKSPACE=yourlab && \
        ansible-playbook playbooks/tf_core.yml && \
        ansible-playbook playbooks/core_init.yml && \
        ansible-playbook playbooks/gandi_delegate_subdomain.yml -e mode=destroy -e force=true && \
        ansible-playbook playbooks/gandi_delegate_subdomain.yml && \
        ansible-playbook playbooks/core_system.yml && \
        ansible-playbook playbooks/acme_rotate_certificates.yml && \
        ansible-playbook playbooks/core_rancher.yml && \
        ansible-playbook playbooks/tf_rancher_bootstrap.yml

.. admonition:: SEASONED SCRIPT - DESTROY
    :class: danger

    .. code:: bash

        export KUBEFACTORY_WORKSPACE=yourlab && \
        ansible-playbook playbooks/gandi_delegate_subdomain.yml -e mode=destroy -e force=true && \
        ansible-playbook playbooks/tf_core.yml -e tf_action=destroy
