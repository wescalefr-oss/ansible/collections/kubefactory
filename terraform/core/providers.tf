terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      version = "2.2.2"
    }
  }
}
