locals {
  k8s_cluster_name = terraform.workspace

  k8s_cluster_version          = var.k8s_cluster_version
  k8s_cluster_cni              = var.k8s_cluster_cni
  k8s_cluster_scw_ingress      = "none"
  k8s_cluster_enable_dashboard = false

  k8s_ingress_namespace = "ingress"

  k8s_pool_name = "${local.k8s_cluster_name}_pool"

  k8s_pool_size      = var.k8s_pool_size
  k8s_pool_node_type = var.k8s_pool_node_type

  traefik_pvc_name = "traefik-data"
  traefik_pvc_size = "1Gi"
  traefik_name = "traefik"
  k8s_kubeconfig = scaleway_k8s_cluster.cluster.kubeconfig[0].config_file
}

resource "local_file" "cluster_kubeconfig" {
    content     = local.k8s_kubeconfig
    filename = var.k8s_kubeconfig_output_file
}

resource "scaleway_k8s_cluster" "cluster" {
  name             = local.k8s_cluster_name
  version          = local.k8s_cluster_version
  cni              = local.k8s_cluster_cni
}

resource "scaleway_k8s_pool" "cluster_pool" {
  cluster_id = scaleway_k8s_cluster.cluster.id

  name      = local.k8s_pool_name
  node_type = local.k8s_pool_node_type
  size      = local.k8s_pool_size
  container_runtime = "containerd"
}

resource "null_resource" "kubeconfig" {
  depends_on = [scaleway_k8s_pool.cluster_pool] # at least one pool here
  triggers = {
    host                   = scaleway_k8s_cluster.cluster.kubeconfig[0].host
    token                  = scaleway_k8s_cluster.cluster.kubeconfig[0].token
    cluster_ca_certificate = scaleway_k8s_cluster.cluster.kubeconfig[0].cluster_ca_certificate
  }
}

provider "helm" {
  kubernetes {
    host = null_resource.kubeconfig.triggers.host
    token = null_resource.kubeconfig.triggers.token
    cluster_ca_certificate = base64decode(null_resource.kubeconfig.triggers.cluster_ca_certificate)
  }
}

resource "scaleway_lb_ip" "nginx_ip" {
  zone       = "fr-par-1"
  project_id = scaleway_k8s_cluster.cluster.project_id
}

resource "helm_release" "nginx_ingress" {
  name      = "nginx-ingress"
  namespace = "kube-system"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart = "ingress-nginx"

  set {
    name = "controller.service.loadBalancerIP"
    value = scaleway_lb_ip.nginx_ip.ip_address
  }

  // enable proxy protocol to get client ip addr instead of loadbalancer one
  set {
    name = "controller.config.use-proxy-protocol"
    value = "true"
  }
  set {
    name = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/scw-loadbalancer-proxy-protocol-v2"
    value = "true"
  }

  // indicates in which zone to create the loadbalancer
  set {
    name = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/scw-loadbalancer-zone"
    value = scaleway_lb_ip.nginx_ip.zone
  }

  // enable to avoid node forwarding
  set {
    name = "controller.service.externalTrafficPolicy"
    value = "Local"
  }

  // enable this annotation to use cert-manager
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/scw-loadbalancer-use-hostname"
    value = "true"
  }

  set {
    name  = "tcp.4222"
    value = "guru/veilleurs:4222" 
  }

}
