output "full_id" {
  value = scaleway_k8s_cluster.cluster.id
}

output "lb_ip" {
  value = scaleway_lb_ip.nginx_ip.ip_address
}

output "short_id" {
  value = split("/", scaleway_k8s_cluster.cluster.id)[1]
}

output "apiserver_url" {
  value = scaleway_k8s_cluster.cluster.apiserver_url
}

output "wildcard_dns" {
  value = scaleway_k8s_cluster.cluster.wildcard_dns
}

output "kubeconfig_file" {
  value = scaleway_k8s_cluster.cluster.kubeconfig[0].config_file
}

output "kubeconfig_host" {
  value = scaleway_k8s_cluster.cluster.kubeconfig[0].host
}

output "kubeconfig_token" {
  value = scaleway_k8s_cluster.cluster.kubeconfig[0].token
}

output "kubeconfig_cluster_ca_certificate" {
  value = scaleway_k8s_cluster.cluster.kubeconfig[0].cluster_ca_certificate
}
