variable "k8s_cluster_version" {
  default = "1.23.6"
}

variable "k8s_cluster_cni" {
  default = "calico"
}

variable "k8s_pool_size" {
  default = 4
}

variable "k8s_pool_node_type" {
  default = "DEV1_M"
}

variable "k8s_kubeconfig_output_file" {}
