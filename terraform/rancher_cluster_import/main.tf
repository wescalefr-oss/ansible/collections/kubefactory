locals {
  cluster_name = var.cluster_name
}

resource "rancher2_cluster" "imported" {
  name = local.cluster_name
}
