output "registration_command" {
  value = rancher2_cluster.imported.cluster_registration_token.0.command
}

output "registration_manifest_url" {
  value = rancher2_cluster.imported.cluster_registration_token.0.manifest_url
}
