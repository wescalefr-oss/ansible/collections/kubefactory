locals {
  cluster_name = var.cluster_name

  lb_ip = var.lb_ip
  rancher_logging_namespace    = "cattle-logging-system"
  rancher_monitoring_namespace = "cattle-monitoring-system"
  rancher_repo_name            = "rancher-charts"

  grafana_catalog_name = "grafana"
  grafana_catalog_url  = "https://grafana.github.io/helm-charts"

  loki_rancher_app_name = "loki"
  loki_namespace        = local.rancher_logging_namespace
  loki_repo_name        = local.grafana_catalog_name
  loki_chart_name       = "loki"

  bitnami_catalog_name = "bitnami"
  bitnami_catalog_url  = "https://charts.bitnami.com/bitnami"

  monitoring_rancher_app_name = "rancher-monitoring"
  monitoring_namespace        = local.rancher_monitoring_namespace
  monitoring_repo_name        = local.rancher_repo_name
  monitoring_chart_name       = "rancher-monitoring"
  monitoring_values           = file("${abspath(path.root)}/values/monitoring.values.yml")

  grafana_dashboard_namespace = "cattle-dashboards"

  logging_rancher_app_name = "rancher-logging"
  logging_namespace        = local.rancher_logging_namespace
  logging_repo_name        = local.rancher_repo_name
  logging_chart_name       = "rancher-logging"
  logging_values           = file("${abspath(path.root)}/values/logging.values.yml")

  cert_manager_catalog_name = "jetstack"
  cert_manager_catalog_url  = "https://charts.jetstack.io"

  cert_manager_app_name   = "cert-manager"
  cert_manager_namespace  = "kube-system"
  cert_manager_repo_name  = local.cert_manager_catalog_name
  cert_manager_chart_name = local.cert_manager_app_name
  cert_manager_values     = file("${abspath(path.root)}/values/cert-manager.values.yml")

  ingress_app_name      = "nginx-ingress"
  ingress_namespace     = "ingress"
  ingress_catalog_url   = "https://helm.nginx.com/stable"
  ingress_repo_name     = "nginx-ingress"
  ingress_chart_name    = local.ingress_app_name
  ingress_chart_version = "0.13.2"
  ingress_lb_service_name = "${local.ingress_app_name}-controller"

  letsencrypt_contact_email = "gitlab.drone@wescale.fr"
}

data "rancher2_cluster" "target" {
  name = local.cluster_name
}

resource "rancher2_catalog_v2" "grafana" {
  cluster_id = data.rancher2_cluster.target.id

  name = local.grafana_catalog_name
  url  = local.grafana_catalog_url
}

resource "rancher2_catalog_v2" "bitnami" {
  cluster_id = data.rancher2_cluster.target.id

  name = local.bitnami_catalog_name
  url  = local.bitnami_catalog_url
}

resource "rancher2_catalog_v2" "jetstack" {
  cluster_id = data.rancher2_cluster.target.id

  name = local.cert_manager_catalog_name
  url  = local.cert_manager_catalog_url
}

resource "rancher2_app_v2" "loki" {
  cluster_id = data.rancher2_cluster.target.id

  name       = local.loki_rancher_app_name
  namespace  = local.loki_namespace
  repo_name  = local.loki_repo_name
  chart_name = local.loki_chart_name

  depends_on = [rancher2_catalog_v2.grafana]
}

resource "rancher2_app_v2" "logging" {
  cluster_id = data.rancher2_cluster.target.id
  name       = local.logging_chart_name
  namespace  = local.logging_namespace
  repo_name  = local.logging_repo_name
  chart_name = local.logging_chart_name
#  values     = local.logging_values
}

resource "rancher2_app_v2" "monitoring" {
  cluster_id = data.rancher2_cluster.target.id

  name       = local.monitoring_rancher_app_name
  namespace  = local.monitoring_namespace
  repo_name  = local.monitoring_repo_name
  chart_name = local.monitoring_chart_name
#  values     = local.monitoring_values
}

resource "rancher2_app_v2" "cert_manager" {
  cluster_id = data.rancher2_cluster.target.id

  name       = local.cert_manager_app_name
  namespace  = local.cert_manager_namespace
  repo_name  = local.cert_manager_repo_name
  chart_name = local.cert_manager_chart_name

  values = data.template_file.cert_manager_values.rendered

  depends_on = [rancher2_catalog_v2.jetstack]
}



data "kubernetes_service" "lb" {

  metadata {
    name      = local.ingress_lb_service_name
    namespace = local.ingress_namespace
  }

#  depends_on = [helm_release.ingress]
}

data "template_file" "letsencrypt_production" {
  template = file("${abspath(path.root)}/manifests/letsencrypt-production.tpl")
  vars = {
    letsencrypt_contact_email = local.letsencrypt_contact_email
  }
}

data "template_file" "cert_manager_values" {
  template = file("${abspath(path.root)}/values/cert-manager.values.yml")
}


resource "kubectl_manifest" "letsencrypt_production" {
    yaml_body = data.template_file.letsencrypt_production.rendered

    depends_on = [rancher2_app_v2.cert_manager]
}

data "template_file" "letsencrypt_staging" {
  template = file("${abspath(path.root)}/manifests/letsencrypt-staging.tpl")
  vars = {
    letsencrypt_contact_email = local.letsencrypt_contact_email
  }
}

resource "kubectl_manifest" "letsencrypt_staging" {
    yaml_body = data.template_file.letsencrypt_staging.rendered

    depends_on = [rancher2_app_v2.cert_manager]
}

data "template_file" "logging_flow" {
  template = file("${abspath(path.root)}/manifests/logging/logging-cluster-flow.tpl")
  vars = {
    namespace = local.loki_namespace
  }

  depends_on = [rancher2_app_v2.loki]
}

resource "kubectl_manifest" "logging_flow" {
    yaml_body = data.template_file.logging_flow.rendered

    depends_on = [rancher2_app_v2.loki]
}

data "template_file" "cluster_output" {
  template = file("${abspath(path.root)}/manifests/logging/logging-cluster-output.tpl")
  vars = {
    namespace = local.loki_namespace
  }

  depends_on = [rancher2_app_v2.loki]
}

resource "kubectl_manifest" "cluster_output" {
    yaml_body = data.template_file.cluster_output.rendered

    depends_on = [rancher2_app_v2.loki]
}

data "template_file" "loki_grafana_datasource" {
  template = file("${abspath(path.root)}/manifests/grafana/loki-grafana-ds.tpl")
  vars = {
    loki_namespace = local.loki_namespace
    grafana_namespace = local.monitoring_namespace
  }

  depends_on = [rancher2_app_v2.loki]
}

resource "kubectl_manifest" "loki_grafana_datasource" {
    yaml_body = data.template_file.loki_grafana_datasource.rendered

    depends_on = [rancher2_app_v2.monitoring]
}

data "template_file" "nats_grafana_dashboard" {
  template = file("${abspath(path.root)}/manifests/grafana/nats-grafana-dashboard.tpl")
  vars = {
    grafana_dashboard_namespace = local.grafana_dashboard_namespace
  }
}

resource "kubectl_manifest" "nats_grafana_dashboard" {
    yaml_body = data.template_file.nats_grafana_dashboard.rendered

    depends_on = [rancher2_app_v2.monitoring]
}

data "template_file" "nginx_grafana_dashboard" {
  template = file("${abspath(path.root)}/manifests/grafana/nginx-grafana-dashboard.tpl")
  vars = {
    grafana_dashboard_namespace = local.grafana_dashboard_namespace
  }

  depends_on = [rancher2_app_v2.monitoring]
}

resource "kubectl_manifest" "nginx_grafana_dashboard" {
    yaml_body = data.template_file.nginx_grafana_dashboard.rendered

    depends_on = [rancher2_app_v2.monitoring]
}

data "template_file" "redis_grafana_dashboard" {
  template = file("${abspath(path.root)}/manifests/grafana/redis-grafana-dashboard.tpl")
  vars = {
    grafana_dashboard_namespace = local.grafana_dashboard_namespace
  }

  depends_on = [rancher2_app_v2.monitoring]
}

resource "kubectl_manifest" "redis_grafana_dashboard" {
    yaml_body = data.template_file.redis_grafana_dashboard.rendered

    depends_on = [rancher2_app_v2.monitoring]
}
