apiVersion: v1
kind: ConfigMap
metadata:
  name: loki-grafana-datasource
  namespace: ${grafana_namespace}
  labels:
    grafana_datasource: "1"
data:
  datasources.yaml: |-
    apiVersion: 1
    datasources:
    - name: internal-loki
      type: loki
      access: proxy
      url: "http://loki.${loki_namespace}.svc.cluster.local:3100"
      basicAuth: false
      isDefault: false