---
apiVersion: logging.banzaicloud.io/v1beta1
kind: ClusterFlow
metadata:
  name: all2loki
  namespace: ${namespace}
spec:
  globalOutputRefs:
  - loki