---
apiVersion: logging.banzaicloud.io/v1beta1
kind: ClusterOutput
metadata:
  name: loki
  namespace: ${namespace}
spec:
  loki:
    configure_kubernetes_labels: true
    extract_kubernetes_labels: true
    url: http://loki.${namespace}.svc.cluster.local:3100