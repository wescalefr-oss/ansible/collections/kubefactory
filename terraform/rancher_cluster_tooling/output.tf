output "rancher_id" {
  value = data.rancher2_cluster.target.id
}

output "rancher_name" {
  value = data.rancher2_cluster.target.name
}

#output "load_balancer_ip" {
#  value = data.kubernetes_service.lb.status.0.load_balancer.0.ingress.0.ip
#}
