terraform {
  required_providers {
    rancher2 = {
      source  = "rancher/rancher2"
      version = "1.11.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}

provider "kubectl" {
  config_path = var.k8s_kubeconfig
}

provider "kubernetes" {
  config_path = var.k8s_kubeconfig
}

provider "helm" {
  kubernetes {
    config_path = var.k8s_kubeconfig
  }
}
